package com.sda.refactoring;

import org.junit.Assert;
import org.junit.Test;

public class BinaryShitTreeTest {

    @Test
    public void shouldInsertNode () {
        // given
        BinaryTree<Integer> treeRight = new BinaryTree<Integer>();
        BinaryTree<Integer> treeLeft = new BinaryTree<Integer>();
        for (int i = 0; i < 5; i++) {
            treeRight.insertNode(i);
            treeLeft.insertNode(5-i);
        }
        boolean result = true;

        // when
        TreeNode<Integer> leftNode = treeLeft.getRoot();
        TreeNode<Integer> rightNode = treeRight.getRoot();

        for (int i = 0; i < 5; i++) {
            if (leftNode.element.compareTo(5 - i) != 0 || rightNode.element.compareTo(i) != 0 ) result = false;
            leftNode = leftNode.left;
            rightNode = rightNode.right;
        }

        // then
        Assert.assertTrue("DUPA", result);

    }

}