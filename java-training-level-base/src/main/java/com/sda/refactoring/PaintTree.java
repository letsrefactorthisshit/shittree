package com.sda.refactoring;

import javax.swing.*;
import java.awt.*;

class PaintTree<E extends Comparable<E>> extends JPanel {
    public int radius = 20;
    public int vGap = 50;
    private BinaryTree<E> tree;

    public PaintTree(BinaryTree<E> tree) {
        this.tree = tree;
        radius = 20;
        vGap = 50;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (tree.getRoot() != null) {
            displayTree(g, tree.getRoot(), getWidth() / 2, 30, getWidth() / 4);
        }
    }

    public void displayTree(Graphics g, TreeNode root, int x, int y, int hGap) {
        g.drawOval(x - radius, y - radius, 2 * radius, 2 * radius);
        g.drawString(root.element + "", x - 6, y + 4);

        if (root.left != null) {
            connectLeftChild(g, x - hGap, y + vGap, x, y);
            displayTree(g, root.left, x - hGap, y + vGap, hGap / 2);
        }

        if (root.right != null) {
            connectRightChild(g, x + hGap, y + vGap, x, y);
            displayTree(g, root.right, x + hGap, y + vGap, hGap / 2);
        }
    }

    public void connectLeftChild(Graphics g, int x1, int y1, int x2, int y2) {
        double d = Math.sqrt(vGap * vGap + (x2 - x1) * (x2 - x1));
        int x11 = (int) (x1 + radius * (x2 - x1) / d);
        int y11 = (int) (y1 - radius * vGap / d);
        int x21 = (int) (x2 - radius * (x2 - x1) / d);
        int y21 = (int) (y2 + radius * vGap / d);
        g.drawLine(x11, y11, x21, y21);
    }

    public void connectRightChild(Graphics g, int x1, int y1, int x2, int y2) {
        double d = Math.sqrt(vGap * vGap + (x2 - x1) * (x2 - x1));
        int x11 = (int) (x1 - radius * (x1 - x2) / d);
        int y11 = (int) (y1 - radius * vGap / d);
        int x21 = (int) (x2 + radius * (x1 - x2) / d);
        int y21 = (int) (y2 + radius * vGap / d);
        g.drawLine(x11, y11, x21, y21);
    }
}