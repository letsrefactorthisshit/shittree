package com.sda.refactoring;

class PreOrderedIterator<E extends Comparable<E>> implements java.util.Iterator {

    public java.util.ArrayList<E> list = new java.util.ArrayList<E>();
    public int current = 0;
    private BinaryTree<E> tree;

    /**
     * Konstruktor befuellt die Liste in der alle Elemente des Baums gespeichert werden
     */
    public PreOrderedIterator(BinaryTree<E> tree) {
        this.tree = tree;
        preOrder(); // Traversiert durch den Baum und speichert die Elemente in einer Liste
    }

    /**
     * Geht traversal vom Wurzelknoten aus nach unten
     */
    public void preOrder() {
        preOrder(tree.getRoot());
    }

    /**
     * Geht traversal vom einem gegeben Knoten aus nach unten
     */
    public void preOrder(TreeNode<E> root) {
        if (root == null) {
            return;
        }
        list.add(root.element);
        preOrder(root.left);
        preOrder(root.right);
    }

    /**
     * Gibt es noch weitere Elemente?
     */
    public boolean hasNext() {
        if (current < list.size()) {
            return true;
        }

        return false;
    }

    /**
     * Gibt das aktuelle element zurueck und zaehlt weiter zum naechsten
     */
    public Object next() {
        return list.get(current++);
    }

    /**
     * Entfernt das aktuelle Element und laed den Baum neu
     */
    public void remove() {
        tree.delete(list.get(current), true); // Delete the current element
        list.clear(); // Clear the list
        tree.inOrder(); // Rebuild the list
    }
}