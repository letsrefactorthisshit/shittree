package com.sda.refactoring;

import java.util.Iterator;

public class BinaryTree<E extends Comparable<E>> {
    public TreeNode<E> root;
    TreeNode<E> parent;
    TreeNode<E> current;
    private int size = 0;
    boolean isNodeFound;

    BinaryTree() {
    }

    boolean search(E e) {

        navigate(e);

        if (isNodeFound == true)  {
            return true;
        } else {
            return false;

        }
    }

    void insertNode(E e) {
        if (root == null) {
            root = new TreeNode<>(e);
            return;
        }

        navigate(e);

        if (e.compareTo(parent.element) < 0) {
            parent.left = new TreeNode<>(e);
        } else {
            parent.right = new TreeNode<>(e);
        }

        size++;
    }


    public int getDepth(TreeNode<E> node) {
        if (node == null) {
            return (0);
        } else {
            int lDepth = getDepth(node.left);
            int rDepth = getDepth(node.right);

            if (lDepth > rDepth) {
                return (lDepth + 1);
            } else {
                return (rDepth + 1);
            }
        }
    }


    public void inOrder() {
        inOrder(root);
    }


    protected void inOrder(TreeNode<E> root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        System.out.print(root.element + " ");
        inOrder(root.right);
    }


    public void postOrder() {
        postOrder(root);
    }


    protected void postOrder(TreeNode<E> root) {
        if (root == null) {
            return;
        }
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.element + " ");
    }

    public void preOrder() {
        preOrder(root);
    }


    protected void preOrder(TreeNode<E> root) {
        if (root == null) {
            return;
        }
        System.out.print(root.element + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    public int getSize() {
        return this.size;
    }


    public TreeNode getRoot() {
        return this.root;
    }


    public void saveToFile() {
        try {
            java.io.PrintWriter writer = new java.io.PrintWriter("output.dat", "UTF-8");
            Iterator<String> iterator = this.iterator();
            while (iterator.hasNext()) {
                writer.println(iterator.next().toString());
            }
            writer.close();
        } catch (Exception xxxx) {
            xxxx.printStackTrace();
        }
    }

    public void loadFromFile() {
        try {
            java.io.FileReader in = new java.io.FileReader("output.dat");
            java.io.BufferedReader br = new java.io.BufferedReader(in);
            this.clear();
            String elem;
            while ((elem = br.readLine()) != null) {
                insertNode((E) (Object) elem);
            }
            in.close();
        } catch (Exception xxxx) {
            xxxx.printStackTrace();
        }
    }

    public boolean delete(E e, boolean remove) {

        navigate(e);

        if (current == null) {
            return false;
        }

        if (current.left == null) {
            if (parent == null) {
                root = current.right;
            } else {
                if (e.compareTo(parent.element) < 0) {
                    parent.left = current.right;
                } else {
                    parent.right = current.right;
                }
            }
        } else {

            TreeNode<E> parentOfRightMost = current;
            TreeNode<E> rightMost = current.left;

            while (rightMost.right != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.right;
            }

            current.element = rightMost.element;

            if (parentOfRightMost.right == rightMost) {
                parentOfRightMost.right = rightMost.left;
            } else

            {
                parentOfRightMost.left = rightMost.left;
            }
        }
        size--;
        return true;
    }


    java.util.Iterator iterator() {
        return new PreOrderedIterator(this);
    }

    void clear() {
        root = null;
        size = 0;
    }

    private void navigate(E e) {

        parent = null;
        current = root;
        isNodeFound = false;

        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            } else if (e.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            } else if (e.compareTo(current.element) == 0){
                isNodeFound = true;
                break;
            }
        }
    }

}
