package com.sda.refactoring;

class TreeNode<E extends Comparable<E>> {
    E element;
    TreeNode<E> left;
    TreeNode<E> right;

    TreeNode(E e) {
        this.element = e;
    }
}