package com.sda.refactoring;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class BinaryShitTree extends JPanel {
    // fields
    public BinaryTree<Integer> tree; // Der BinaryTree der dargestellt bzw. befuellt wird
    public JTextField jtfKey = new JTextField(5);
    public PaintTree paintTree;
    //Buttons
    public JButton jbt1 = new JButton("Einf�gen");
    public JButton jbt2 = new JButton("L�schen");
    public JButton jbt3 = new JButton("Speichern");
    public JButton jbt4 = new JButton("Laden");
    public JButton jbt5 = new JButton("Reset");
    protected JButton jbt6 = new JButton("Neue");
    protected JLabel jlbDepth = new JLabel("0");
    protected JLabel jlbSize2 = new JLabel("0");
    public JLabel jlbSize = new JLabel("0");
    public boolean isFertig = false;



    private BinaryShitTree(BinaryTree<Integer> tree) {
        this.tree = tree; // Set a binary tree to be displayed
        paintTree = new PaintTree(tree);
        setUI();
    }

    /**
     * Initialisiert das UI fuer den Binary Tree
     */
    private void setUI() {
        this.setLayout(new BorderLayout());
        add(paintTree, BorderLayout.CENTER);
        JPanel panel = new JPanel();
        // Die vielen Leerzeichen in den JLabels kommen von Schwierigkeiten mit dem Layout
        // Kann man auch besser l�sen
        panel.add(new JLabel("Nodes: "));
        panel.add(jlbSize);
        panel.add(new JLabel("  Tiefe: "));
        panel.add(jlbDepth);
        panel.add(new JLabel("        Wert: "));
        panel.add(jtfKey);
        panel.add(jbt1);
        panel.add(jbt2);
        panel.add(new JLabel("    "));
        panel.add(jbt3);
        panel.add(jbt4);
        panel.add(new JLabel("    "));
        panel.add(jbt5);
        add(panel, BorderLayout.SOUTH);
        repaintButtons();

        //Event Listener fuer die Buttons
        jbt1.addActionListener(e -> {
            Integer key = Integer.parseInt(jtfKey.getText());
            if (tree.search(key)) { // Key wurde im Baum gefunden
                JOptionPane.showMessageDialog(null, key + " ist bereits vorhanden");
            }
            else {
                if (key < 1000 && key > 0) {
                    try {
                        tree.insertNode(key);
                    }
                    catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    paintTree.repaint();
                    repaintButtons();
                }
                else {
                    JOptionPane.showMessageDialog(null, "Der Wert ist zu lang oder leer");
                }
            }
        });

        jbt2.addActionListener(e -> {
            Integer key = Integer.parseInt(jtfKey.getText());
            if (!tree.search(key)) { // key is not in the tree
                JOptionPane.showMessageDialog(null,
                        key + " ist nicht vorhanden");
            }
            else {
                tree.delete(key, true); // Delete a key
                paintTree.repaint(); // Redisplay the tree
                repaintButtons();
            }
        });

        jbt3.addActionListener(e -> {
            tree.saveToFile();
            JOptionPane.showMessageDialog(null, "Baum wurde gespeichert.");
        });

        jbt4.addActionListener(e -> {
            tree.loadFromFile();
            paintTree.repaint(); // Redisplay the tree
            repaintButtons();
            JOptionPane.showMessageDialog(null, "Baum wurde geladen.");
        });

        jbt5.addActionListener(e -> {
            tree.clear();
            paintTree.repaint(); // Redisplay the tree
            repaintButtons();
        });


    }

    public void repaintButtons() {
        jlbDepth.setText(String.valueOf(tree.getDepth(tree.getRoot())));
        jlbSize.setText(String.valueOf(tree.getSize()));
    }

    // Eingebette Klasse PaintTree, die die Darstellung der Nodes und die Verbindungslinien zeichnet


    /**
     * Mainmethode setzt das Ganze in Gang
     * Hier kann auch ein bereits vorhandener BinaryTree uebergeben werden
     */
    public static void main(String[] args) {
        System.out.println("-----");
        JFrame frame = new JFrame("Binary Tree");
        JPanel applet = new BinaryShitTree(new BinaryTree<Integer>()); //Leerer BinaryTree wird dem Konstruktor �bergeben
        frame.add(applet);
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
