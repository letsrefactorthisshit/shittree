package com.sda.collection;

import java.util.Arrays;

public class PlanetClass {

    public final static PlanetClass EARTH = new PlanetClass("EARTH");
    public final static PlanetClass MARS = new PlanetClass("MARS");

    String name;

    private PlanetClass(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public static void valueOf(String value) {
        try {
            PlanetClass.class.getField(value).get(PlanetClass.class);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

//    public static PlanetClass values() {
//        //TODO: na poniedzialek
//    }
}
